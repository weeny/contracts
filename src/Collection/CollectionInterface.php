<?php

namespace Weeny\Contract\Collection;

/**
 * Interface CollectionInterface
 * @package Weeny\Contract\Collection
 */
interface CollectionInterface extends \Iterator, \ArrayAccess, \Countable
{
    /**
     * Return true if $value already contained in collection
     * @param $value
     * @return bool
     */
    public function contains($value): bool;

    /**
     * Push value to collection
     * @param $value
     * @return mixed
     * @throws \TypeError
     */
    public function push($value): void;

    /**
     * Clears the collection, removing all elements.
     */
    public function clear(): void;

    /**
     * Checks whether the collection is empty (contains no elements).
     *
     * @return bool TRUE if the collection is empty, FALSE otherwise.
     */
    public function isEmpty(): bool;

    /**
     * Removes the element at the specified index from the collection.
     *
     * @param int $index The index of the element to remove.
     *
     * @return mixed The removed element or NULL, if the collection did not contain the element.
     */
    public function removeByIndex(int $index);

    /**
     * Removes the specified value from the collection, if it is found.
     * @param mixed $value The value to remove.
     * @return bool TRUE if this collection contained the specified value, FALSE otherwise.
     * @return bool
     * @throws \TypeError
     */
    public function removeByValue($value): bool;

    /**
     * Pop the element off the end of array
     * @return mixed
     */
    public function pop();

    /**
     * Shift an element off the beginning of array
     * @return mixed
     */
    public function shift();

    /**
     * Prepend one or more elements to the beginning of an array
     * @param mixed ...$values
     * @return mixed
     * @throws \TypeError
     */
    public function unShift(...$values);

    /**
     * Return as array
     * @return array
     */
    public function toArray(): array;

    /**
     * Reverce order of elements
     * @return mixed
     */
    public function reverce(): void;

    /**
     * Removes duplicate values from an array
     */
    public function unique(): void;

    /**
     * Return index position of $value or null if collection isn't contains value
     * @return int|null
     */
    public function indexOf($value): ?int;
}