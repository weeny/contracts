<?php

namespace Weeny\Contract\Collection;

interface StringCollectionInterface extends CollectionInterface
{
    /**
     * @return string
     */
    public function current(): string;

    /**
     * @param mixed $offset
     * @return string
     */
    public function offsetGet($offset): string;

    /**
     * @param int $index
     * @return string
     */
    public function removeByIndex(int $index): ?string;

    /**
     * @inheritDoc
     */
    public function pop(): string;

    /**
     * @inheritDoc
     */
    public function shift(): string;
}