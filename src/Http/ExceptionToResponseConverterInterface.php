<?php

namespace Weeny\Contract\Http;

use Psr\Http\Message\ResponseInterface;

interface ExceptionToResponseConverterInterface
{

    /**
     * Convert exception to correct http response object
     * @param \Throwable $exception
     * @return ResponseInterface
     */
    public function convertToResponse(\Throwable $exception): ResponseInterface;

}