<?php

namespace Weeny\Contract\Http\Exceptions;

use Weeny\Contract\Exceptions\WeenyExceptionInterface;

interface HttpExceptionInterface extends \Throwable, WeenyExceptionInterface
{

    /**
     * Return code using in http response
     * @return int
     */
    public function getHttpCode(): int;
}