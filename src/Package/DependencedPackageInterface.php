<?php

namespace Weeny\Contract\Package;

use Weeny\Contract\Collection\StringCollectionInterface;

interface DependencedPackageInterface extends PackageInterface
{
    /**
     * Returning dependent package class names
     * @return StringCollectionInterface
     */
    public function getDependencyPackageNames(): StringCollectionInterface;
}