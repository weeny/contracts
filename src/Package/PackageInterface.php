<?php

namespace Weeny\Contract\Package;

interface PackageInterface
{

    /**
     * Return short name of package
     * @return string
     */
    public function getShortName(): string;
}