<?php

namespace Weeny\Contract\Package;

use Weeny\Contract\Collection\CollectionInterface;

interface PackageCollectionInterface extends CollectionInterface
{
    /**
     * @return string
     */
    public function current(): PackageInterface;

    /**
     * @param mixed $offset
     * @return string
     */
    public function offsetGet($offset): PackageInterface;

    /**
     * @inheritDoc
     */
    public function removeByIndex(int $index): ?PackageInterface;

    /**
     * @inheritDoc
     */
    public function pop(): PackageInterface;

    /**
     * @inheritDoc
     */
    public function shift(): PackageInterface;

    /**
     * Return the collection contain only instance of $className elements
     * @param string $className
     * @return PackageCollectionInterface
     */
    public function filterInstanceOf(string $className): PackageCollectionInterface;
}