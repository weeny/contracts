<?php

namespace Weeny\Contract\Package;

use Weeny\Contract\Container\ExternalContainerBuilderInterface;

interface ContainerBuilderPackageInterface extends PackageInterface
{

    /**
     * Get builder of container with preloaded configuration from packages
     * @param PackageCollectionInterface $packages
     * @return ExternalContainerBuilderInterface
     */
    public function loadExternalContainerBuilder(PackageCollectionInterface $packages): ExternalContainerBuilderInterface;

}