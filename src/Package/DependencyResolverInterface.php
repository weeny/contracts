<?php

namespace Weeny\Contract\Package;

use Weeny\Contract\Exceptions\CircularReferenceWatcherExceptionInterface;
use Weeny\Contract\Exceptions\WeenyExceptionInterface;
use Weeny\Contract\Package\Exceptions\PackageInitializationExceptionInterface;
use Weeny\Contract\Package\Exceptions\PackageNotFoundExceptionInterface;

interface DependencyResolverInterface
{

    /**
     * Get collection of packages with them dependencies

     * @return PackageCollectionInterface
     * @throws CircularReferenceWatcherExceptionInterface
     * @throws PackageInitializationExceptionInterface
     * @throws PackageNotFoundExceptionInterface
     * @throws WeenyExceptionInterface
     */
    public function resolveDependencies(PackageInterface ...$packages): PackageCollectionInterface;
}