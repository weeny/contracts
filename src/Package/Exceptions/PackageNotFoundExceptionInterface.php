<?php

namespace Weeny\Contract\Package\Exceptions;

interface PackageNotFoundExceptionInterface extends PackageInitializationExceptionInterface
{

}