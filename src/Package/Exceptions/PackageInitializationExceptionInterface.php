<?php

namespace Weeny\Contract\Package\Exceptions;

use Weeny\Contract\Exceptions\WeenyExceptionInterface;

interface PackageInitializationExceptionInterface extends WeenyExceptionInterface
{
    /**
     * Returning name of package
     * @return string
     */
    public function getPackageName(): string;
}