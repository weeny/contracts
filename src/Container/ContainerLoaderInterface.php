<?php

namespace Weeny\Contract\Container;

use Psr\Container\ContainerInterface;
use Weeny\Contract\Container\Exceptions\CircularReferenceExceptionsInterface;
use Weeny\Contract\Container\Exceptions\ContainerConfigurationExceptionInterface;
use Weeny\Contract\Container\Exceptions\IncompleteDefinitionServiceExceptionInterface;
use Weeny\Contract\Container\Exceptions\PackagesNotContainContainerBuilderExceptionInterface;
use Weeny\Contract\Container\Exceptions\UnexistsServiceReferenceExceptionInterface;
use Weeny\Contract\Package\PackageCollectionInterface;

interface ContainerLoaderInterface
{
    /**
     * Load service container from packages
     *
     * @param PackageCollectionInterface $pakages
     * @return ContainerInterface
     *
     * @throws PackagesNotContainContainerBuilderExceptionInterface
     * @throws CircularReferenceExceptionsInterface
     * @throws IncompleteDefinitionServiceExceptionInterface
     * @throws ContainerConfigurationExceptionInterface
     * @throws UnexistsServiceReferenceExceptionInterface
     */
    public function loadFromPackagesConfiguration(PackageCollectionInterface $pakages): ContainerInterface;
}