<?php

namespace Weeny\Contract\Container\Exceptions;

use Psr\Container\NotFoundExceptionInterface;

interface ServiceNotFoundExceptionInterface extends ContainerConfigurationExceptionInterface, NotFoundExceptionInterface
{

    /**
     * Return name of problematic service
     * @return string
     */
    public function getServiceName(): string;
}