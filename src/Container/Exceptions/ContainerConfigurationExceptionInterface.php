<?php

namespace Weeny\Contract\Container\Exceptions;

use Psr\Container\ContainerExceptionInterface;
use Weeny\Contract\Exceptions\WeenyExceptionInterface;

interface ContainerConfigurationExceptionInterface extends WeenyExceptionInterface, ContainerExceptionInterface
{

}