<?php

namespace Weeny\Contract\Container\Exceptions;

interface IncompleteDefinitionServiceExceptionInterface extends ContainerConfigurationExceptionInterface
{
    /**
     * Return name of problematic service
     * @return string
     */
    public function getServiceName(): string;
}