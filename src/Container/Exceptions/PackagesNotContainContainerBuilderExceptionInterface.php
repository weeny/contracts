<?php

namespace Weeny\Contract\Container\Exceptions;

use Weeny\Contract\Exceptions\WeenyConfigurationExceptionInterface;

interface PackagesNotContainContainerBuilderExceptionInterface extends WeenyConfigurationExceptionInterface
{

}