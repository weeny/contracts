<?php

namespace Weeny\Contract\Container\Exceptions;

use Psr\Container\NotFoundExceptionInterface;

interface UnexistsServiceReferenceExceptionInterface extends ContainerConfigurationExceptionInterface, NotFoundExceptionInterface
{

}