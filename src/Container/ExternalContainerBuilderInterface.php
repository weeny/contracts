<?php

namespace Weeny\Contract\Container;

use Psr\Container\ContainerInterface;
use Weeny\Contract\Container\Exceptions\CircularReferenceExceptionsInterface;
use Weeny\Contract\Container\Exceptions\ContainerConfigurationExceptionInterface;
use Weeny\Contract\Container\Exceptions\IncompleteDefinitionServiceExceptionInterface;
use Weeny\Contract\Container\Exceptions\UnexistsServiceReferenceExceptionInterface;

interface ExternalContainerBuilderInterface
{
    /**
     * Check container configuration for configuration troubles.
     * Working with inside logic for manipulation with container configuration.
     *
     * Attention! Returned ServiceLocator is incomplete for use as real ServiceLocator,
     * because service definitions of contained may be incomplete, such as
     * with references on non existing services on this container.
     *
     * @return ServiceLocatorInterface
     *
     * @throws CircularReferenceExceptionsInterface
     * @throws IncompleteDefinitionServiceExceptionInterface
     * @throws ContainerConfigurationExceptionInterface
     */
    public function configure(): ServiceLocatorInterface;

    /**
     * Build result container.
     *
     * @param ServiceLocatorInterface $externalServiceLocator Need for compile service references from external container
     *
     * @return ContainerInterface
     *
     * @throws ContainerConfigurationExceptionInterface
     * @throws UnexistsServiceReferenceExceptionInterface
     */
    public function build(ServiceLocatorInterface $externalServiceLocator): ContainerInterface;
}