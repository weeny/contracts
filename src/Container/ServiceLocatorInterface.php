<?php
namespace Weeny\Contract\Container;

use Psr\Container\ContainerInterface;

interface ServiceLocatorInterface extends ContainerInterface
{

    /**
     * @inheritDoc
     */
    public function has($serviceName): bool;

    /**
     * @inheritDoc
     */
    public function get($serviceName);
}